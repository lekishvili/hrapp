require 'rails_helper'

RSpec.describe User, type: :model do
  before { @user = User.new }

  it 'should include the :admin attribute' do
    expect(@user.has_attribute?(:admin)).to be(true)
  end

  context 'when new' do
    it 'should not be a admin' do
      expect(@user).not_to be_admin
    end
  end

  context 'should be a admin' do
    before { @user.admin = true }
    it { expect(@user).to be_admin }
  end
end
