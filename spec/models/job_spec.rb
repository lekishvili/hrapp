require 'rails_helper'

RSpec.describe Job, type: :model do
  let(:user) { FactoryGirl.create(:user) }
  before { @job = FactoryGirl.create(:job) }

  context 'when new' do
    it 'is inactive' do
      expect(@job[:status]).to eq(1)
    end
  end

  describe 'when user_id is not present' do
    before { @job.user_id = nil }
    it { should_not be_valid }
  end

  describe 'when title is not present' do
    before { @job.title = nil }
    it { should_not be_valid }
  end

  describe 'when content is not present' do
    before { @job.content = nil }
    it { should_not be_valid }
  end
end
