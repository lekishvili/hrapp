require 'rails_helper'

RSpec.describe User, type: :view do
  let(:user) { FactoryGirl.create(:user) }
  
  describe 'as not authorized' do
    before { visit root_path logout(:user)}

    it 'has login link' do
      expect(page).to have_link('', href: new_user_session_path)
    end

    it 'has register link' do 
      expect(page).to have_link('', href: new_user_registration_path)
    end
  end

  describe 'as authorized' do 
    before { visit root_path login_as(user) }

    it 'has sign out link' do 
      expect(page).to have_link('', href: destroy_user_session_path)
    end

    it 'has register link' do
      expect(page).to have_link('', new_user_registration_path)
    end

    it 'has new job link' do
      expect(page).to have_link('', new_job_path)
    end

    it 'has user profile link' do
      expect(page).to have_link('', user_profile_index_path(user))
    end
  end
end
