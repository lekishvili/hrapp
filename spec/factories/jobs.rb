FactoryGirl.define do
  factory :job do
    user { FactoryGirl.create(:user) }
    title 'What is lorem ipsum?'
    content 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
  end
end
