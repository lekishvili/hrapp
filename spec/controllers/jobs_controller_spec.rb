require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe JobsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }

  describe 'GET index' do
    before { get :index }

    it 'assigns @jobs' do
      job = FactoryGirl.create(:job)
      expect(assigns(:jobs)).to eq([job])
    end

    it 'renders the index template' do
      expect(response).to render_template :index
    end
  end

  describe 'GET new' do
    before { get(:new) }

    context 'as not authorized' do
      before { logout(:user) }

      it 'redirects to login page' do
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'as authorized' do
      before { login_as(user) }

      it '' do end

      it 'have user_id' do
        expect(assigns(:job).user.id).not_to be nil
      end

      it 'assigns new @job' do
        expect(assigns(:job)).to be_a_new Job
      end

      it 'renders the new template' do
        expect(response).to render_template :new
      end
    end
  end

  describe 'POST create' do
    before do
      login_as(user)
      @job = FactoryGirl.attributes_for(:job)
      post(:create, { job: @job })
    end

    subject { post :create, job: @job }

    context 'with valid attributes' do
      it 'creates new job' do
        expect{ subject }.to change(Job, :count).by 1
      end

      it 'flashes new notice' do
        expect(flash[:notice]).not_to be nil
      end

      it 'redirects to show/:id' do
        expect(response).to redirect_to job_path(assigns(:job))
      end
    end

    context 'with invalid attributes' do
      before do
        login_as(user)
        post(:create, { job: { title: '' } })
      end

      it 'returns new template' do
        expect(response).to render_template :new
      end
    end
  end

  describe 'GET show/:id' do
    before do
      @job = FactoryGirl.create(:job)
      logout(:user)
      get(:show, id: @job.id)
    end

    it 'renders the new template' do
      expect(response).to render_template :show
    end

    it 'assigns @job' do
      expect(assigns(:job)).to eq(@job)
    end
  end

  describe 'GET edit:/id' do
    context 'as not authorized' do
      before do
        @job = FactoryGirl.create(:job)
        get(:edit, id: @job.id)
      end

      it 'redirects to login page' do
        expect(response).to redirect_to new_user_session_path
      end
    end

    context 'as authorized' do
      before do
        sign_in(user)
        @job = FactoryGirl.create(:job)
        get(:edit, id: @job.id)
      end

      it 'renders edit template' do
        expect(response).to render_template :edit
      end

      it 'assigns to @job' do
        expect(assigns(:job)).to eq @job
      end
    end
  end

  describe 'PUT update/:id' do
    let(:attrs) do
      { title: 'lorem', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing' }
    end

    context 'as not authorized' do
      before do
        @job = FactoryGirl.create(:job)
        get(:update, id: @job.id, job: attrs)
      end

      it 'redirects to login page' do
        expect(response).to redirect_to new_user_session_path
      end
    end

    context 'as authorized' do
      context 'with not valid attributes' do
        before do
          attrs[:content] = ''
          sign_in(user)
          @job = FactoryGirl.create(:job)
          get(:update, id: @job.id, job: attrs)
        end

        it 'renders template edit' do
          expect(response).to render_template :edit
        end
      end

      context 'with valid attributes' do
        before do
          sign_in(user)
          @job = FactoryGirl.create(:job)
          get(:update, id: @job.id, job: {title: 'Title', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing'})
        end

        it "redirects to show/:id" do
          expect(response).to redirect_to job_path(@job.id)
        end
      end
    end
  end

end
