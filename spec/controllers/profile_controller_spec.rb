require 'rails_helper'

RSpec.describe ProfileController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }

  describe "GET index" do
    before { get(:index, user_id: user.id) }
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it 'assigns user' do
      expect(assigns(:user)).to eq(user)
    end

    it 'renders index template' do
      expect(response).to render_template(:index)
    end
  end

end
