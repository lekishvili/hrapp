class ProfileController < ApplicationController
  def index
    @user = User.find(params[:user_id])
    @jobs = @user.jobs
  end
end
