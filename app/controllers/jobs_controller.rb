class JobsController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]
  before_filter :find_job, only: [:show, :edit, :update, :destroy]

  def index
    @jobs = Job.all
  end

  def new
    @job = current_user.jobs.new
  end

  def create
    @job = current_user.jobs.new(job_params)

    if @job.save
      flash[:notice] = 'Your job has successfully created'
      redirect_to @job
    else 
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @job.update(job_params)
      redirect_to @job
    else
      render :edit
    end
  end

  def destroy
    @job.destroy
    redirect_to :back
  end

  private

  def job_params
    params.require(:job).permit(:title, :content, :valid_till)
  end

  def find_job
    @job = Job.find(params[:id])
  end

end
