class LogoController < ApplicationController
  before_filter :authenticate_user!, only: [:create]

  def create
    @user = User.find(params[:user_id])
    @user.update_attributes(logo_params)
    redirect_to :back
  end

  private

  def logo_params
    params.require(:user).permit(:logo)
  end

end
