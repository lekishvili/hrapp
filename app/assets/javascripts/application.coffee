#= require jquery
#= require jquery_ujs
#= require turbolinks
#= require bootstrap-sprockets
#= require ckeditor/init
#= require_tree .

$(document).ready ->
  $("#btnToggle").click ->
    if $(this).hasClass("on")
      $("#main .col-md-6").addClass("col-md-4").removeClass "col-md-6"
      $(this).removeClass "on"
    else
      $("#main .col-md-4").addClass("col-md-6").removeClass "col-md-4"
      $(this).addClass "on"
    return

  return
