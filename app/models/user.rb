class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_uploader :logo, LogoUploader

  has_many :jobs
  validates_presence_of :company_name
  validates_length_of   :company_name, minimum: 2, maximum: 15
  validates_length_of   :description, maximum: 1000
  validates_length_of   :url, maximum: 100
end
