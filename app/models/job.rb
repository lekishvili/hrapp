class Job < ActiveRecord::Base
  enum status: [:active, :inactive, :archived]
  belongs_to :user

  validates_presence_of :user_id, :title, :content
  validates_length_of   :title, minimum: 5, maximum: 45
  validates_length_of   :content, minimum: 50, maximum: 50000
  sanitizes             :content
end
