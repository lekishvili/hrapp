class LogoUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  process convert: 'png'
  process tags: ['company_logo']

  version :standard do
    process resize_to_fill: [800, 800, :north]
  end

  version :thumb do
    resize_to_fit(150, 150)
  end
end
