class AddCompanyNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :company_name, :string, null: false, limit: 15
  end
end
