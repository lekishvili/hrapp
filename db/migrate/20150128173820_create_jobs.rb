class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.belongs_to :user, null: false
      t.string :title, null: false, limit: 45
      t.text :content, null: false, limit: 50000
      t.date :valid_till, null: false, default: Time.now + 1.month
      t.integer :status, null: false, default: 1

      t.timestamps null: false
    end

    add_index :jobs, :user_id
  end
end
