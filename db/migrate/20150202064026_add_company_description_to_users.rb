class AddCompanyDescriptionToUsers < ActiveRecord::Migration
  def change
    add_column :users, :description, :text, limit: 1000
  end
end
