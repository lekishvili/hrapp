Rails.application.routes.draw do
  get 'profile/show'

  get 'profile/index'

  devise_for :users, controllers: { registrations: 'registrations' }

  
  root 'jobs#index'
  resources :jobs
  resources :users, only: :none do
    resources :profile, only: [:index]
    resources :logo,    only: [:create]
  end 
end
